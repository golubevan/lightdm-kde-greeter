# Translation of lightdm_theme_userbar.po to Catalan (Valencian)
# Copyright (C) 2024 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# SPDX-FileCopyrightText: 2024 Josep M. Ferrer <txemaq@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: lightdm-kde-greeter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-11-07 00:41+0000\n"
"PO-Revision-Date: 2024-11-07 09:30+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: config.qml:37
#, kde-format
msgid "Background image:"
msgstr "Imatge de fons:"

#: NetworkWidget.qml:24
#, kde-format
msgid "The connection will be active only on the login screen."
msgstr "La connexió només serà activa en la pantalla d'inici de sessió."

#: NetworkWidget.qml:56
#, kde-format
msgid "Disconnect from %1 ?"
msgstr "Voleu desconnectar de %1?"

#: NetworkWidget.qml:59
#, kde-format
msgid "Abort connecting to %1 ?"
msgstr "Voleu interrompre la connexió amb %1?"

#: NetworkWidget.qml:63
#, kde-format
msgid "Connect to %1 ?"
msgstr "Voleu connectar amb %1?"

#: NetworkWidget.qml:67
#, kde-format
msgid "Connect to an insecure access point %1 ?"
msgstr "Voleu connectar amb un punt d'accés insegur %1?"

#: NetworkWidget.qml:72
#, kde-format
msgid "Enter the password to connect to %1"
msgstr "Introduïu la contrasenya per a connectar amb %1"

#: NetworkWidget.qml:77
#, kde-format
msgid "Enter login and password to connect to %1"
msgstr "Introduïu un usuari i contrasenya per a connectar amb %1"

#: NetworkWidget.qml:81 NetworkWidget.qml:87
#, kde-format
msgid "%1: Connection error, try again."
msgstr "%1: S'ha produït un error de connexió, torneu-ho a provar."

#: NetworkWidget.qml:90
#, kde-format
msgid "The security protocol is not supported for %1"
msgstr "El protocol de seguretat no està admés per a %1"

#: NetworkWidget.qml:93
#, kde-format
msgid "Failed to connect to %1"
msgstr "No s'ha pogut connectar amb %1"

#: NetworkWidget.qml:96
#, kde-format
msgid "Error: Can't find access point %1"
msgstr "S'ha produït un error: No s'ha pogut trobar el punt d'accés %1"

#: NetworkWidget.qml:316
#, kde-format
msgid "Login"
msgstr "Usuari"

#: NetworkWidget.qml:331
#, kde-format
msgid "Password"
msgstr "Contrasenya"

#: NetworkWidget.qml:383
#, kde-format
msgid "Disable wireless"
msgstr "Desactiva la xarxa sense fil"

#: NetworkWidget.qml:383
#, kde-format
msgid "Enable wireless"
msgstr "Activa la xarxa sense fil"

#: NetworkWidget.qml:384 NetworkWidget.qml:425
#, kde-format
msgid "Action prohibited"
msgstr "Acció prohibida"

#: NetworkWidget.qml:424
#, kde-format
msgid "Disable networking"
msgstr "Desactiva la xarxa"

#: NetworkWidget.qml:424
#, kde-format
msgid "Enable networking"
msgstr "Activa la xarxa"

#: OtherScreen.qml:27
#, kde-format
msgid "Press %1 to change the primary screen"
msgstr "Premeu %1 per a canviar la pantalla primària"

#: PrimaryScreen.qml:90
#, kde-format
msgid "Login failed"
msgstr "No s'ha pogut fer l'inici de la sessió"

#: PrimaryScreen.qml:483
#, kde-format
msgid "Caps Lock is on"
msgstr "El bloqueig de majúscules està activat"

# skip-rule: k-Num_Lock
#: PrimaryScreen.qml:486
#, kde-format
msgid "Num Lock is on"
msgstr "El bloqueig numèric està activat"

# skip-rule: k-Num_Lock
#: PrimaryScreen.qml:489
#, kde-format
msgid "Caps Lock and Num Lock is on"
msgstr "El bloqueig de majúscules i el bloqueig numèric estan activats"

#: PrimaryScreen.qml:502
#, kde-format
msgid "Log in"
msgstr "Inicia la sessió"

#: PrimaryScreen.qml:521
#, kde-format
msgid "Log in as another user"
msgstr "Inicia la sessió com un altre usuari"

#: PrimaryScreen.qml:680
#, kde-format
msgid "Keyboard layout"
msgstr "Disposició de teclat"

#: PrimaryScreen.qml:725
#, kde-format
msgid "Desktop session"
msgstr "Sessió d'escriptori"

#: PrimaryScreen.qml:752
#, kde-format
msgid "Suspend"
msgstr "Suspén"

#: PrimaryScreen.qml:764
#, kde-format
msgid "Hibernate"
msgstr "Hiberna"

#: PrimaryScreen.qml:777
#, kde-format
msgid "Restart"
msgstr "Reinicia"

#: PrimaryScreen.qml:789
#, kde-format
msgid "Shutdown"
msgstr "Para"

#: PrimaryScreen.qml:819
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Teclat virtual"
